package com.patotski.performance.utils;

import java.util.concurrent.TimeUnit;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

/**
 * Benchmark class courtesy of Viktar Patotski @xpvit
 * https://github.com/xp-vit/java-logging-performance-tests/tree/main/src/jmh/java/com/patotski/performance
 *
 * Supports setting all options here rather than at every class or method.
 */
public class BenchmarkUtils {

  public static void runBenchmark(Class<?> clazz) throws Exception {
    Options opt = new OptionsBuilder()
                .include(clazz.getSimpleName())
                .resultFormat(ResultFormatType.JSON)
                .shouldDoGC(true)
                .result(clazz.getSimpleName() + ".json")
                .jvmArgsAppend("-Djmh.stack.period=1")
                .warmupIterations(5)
                .measurementIterations(5)
                .forks(1)
                .shouldFailOnError(true)
                .timeUnit(TimeUnit.NANOSECONDS)
                .jvmArgs("-server")
                .mode(Mode.AverageTime)
                .threads(1)
                .build();

    new Runner(opt).run();
  }
}
