package com.patotski.performance.bigdec;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

import java.util.regex.Pattern;

import static com.patotski.performance.utils.BenchmarkUtils.runBenchmark;
import java.math.BigDecimal;

/**
 * This is a modified version of Viktar Patotski's BigIntValidationBenchmarks
 *
 * @author Viktar Patotski @xpvit Ken Fogel @omniprof
 */
public class BigDecimalValidationBenchmarks {

    /**
     * Using an inner static class prevents the compiler form optimizing any
     * reading of the static variables
     */
    @State(Scope.Benchmark)
    public static class BigDecState {

        public final String invalid = "42BobTheMoose.0";
        public final String valid = "54321.12345";
        public final String stringPattern = "^[\\d]*[\\.]?[\\d]*$";
        public final Pattern pattern = Pattern.compile(stringPattern);
        public BigDecimal bigDecimal;

        private boolean flag = true;
        private int counter;

        public String getString() {
            counter++;
            if (counter < 20) {
                return valid;
            } else {
                counter = 0;
                return invalid;
            }
        }
    }

    /**
     * Using Exceptions
     *
     * @param state
     * @return
     */
    @Benchmark
    public String exceptionValidation(BigDecState state) {
        String stringToParse = state.getString();
        try {
            state.bigDecimal = new BigDecimal(stringToParse);
        } catch (NumberFormatException e) {
            return "invalid";
        }
        return "valid";
    }

    /**
     * Using a Regular Expression to test for correctness
     *
     * @param state
     * @return
     */
//    @Benchmark
//    public String regExpValidation(BigDecState state) {
//        String stringToParse = state.getString();
//        if (stringToParse.matches(state.stringPattern)) {
//            state.bigDecimal = new BigDecimal(stringToParse);
//            return "valid";
//        }
//        return "invalid";
//    }

    /**
     * Using a pre-compiled Regular Expression to test for correctness
     *
     * @param state
     * @return
     */
    @Benchmark
    public String regExpCompiledPatternValidation(BigDecState state) {
        String stringToParse = state.getString();
        if (state.pattern.matcher(stringToParse).matches()) {
            state.bigDecimal = new BigDecimal(stringToParse);
            return "valid";
        }
        return "invalid";
    }

    /**
     * Where it all begins
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        runBenchmark(BigDecimalValidationBenchmarks.class);
    }
}
